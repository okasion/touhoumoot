{
    "id": "2eaba6d4-4a1b-42ac-bb44-f5e778dca3d4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_heat_bar_shot",
    "eventList": [
        {
            "id": "84418a7e-2840-4122-9d95-09d639b2c72c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2eaba6d4-4a1b-42ac-bb44-f5e778dca3d4"
        },
        {
            "id": "a6aa7655-863a-4d19-801e-4faa0b3c6e6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2eaba6d4-4a1b-42ac-bb44-f5e778dca3d4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e5e93bf9-3ac2-4362-99c1-37edcb6dad3e",
    "visible": true
}