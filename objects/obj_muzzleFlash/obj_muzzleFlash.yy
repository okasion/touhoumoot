{
    "id": "5a6da291-7389-4f31-a8d4-f3d0ea51036a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_muzzleFlash",
    "eventList": [
        {
            "id": "0f075b68-12a4-4d2b-b176-894957d4ecff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5a6da291-7389-4f31-a8d4-f3d0ea51036a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f0a35c89-65ff-464f-951a-e40b6aec1a6a",
    "visible": true
}