{
    "id": "4e5c2a4b-e810-4990-b3ba-fc34c276cd44",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_gap1",
    "eventList": [
        {
            "id": "5e89518c-d229-44fc-b2a5-56fc52c58e25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4e5c2a4b-e810-4990-b3ba-fc34c276cd44"
        },
        {
            "id": "4a11ee27-e251-481c-b5e8-2836e7d7c517",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4e5c2a4b-e810-4990-b3ba-fc34c276cd44"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "01db0d48-3327-489f-9e01-174c314457b2",
    "visible": true
}