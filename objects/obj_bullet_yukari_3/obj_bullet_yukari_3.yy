{
    "id": "0142d7b0-9209-4089-8f8b-fb34d7bcb602",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet_yukari_3",
    "eventList": [
        {
            "id": "b8104e46-7e55-452b-8164-af8aea72276a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0142d7b0-9209-4089-8f8b-fb34d7bcb602"
        },
        {
            "id": "777a9788-d3f6-463d-ad59-72a364c267bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c6f33aa0-f06a-4915-9835-c292b3d700f3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "0142d7b0-9209-4089-8f8b-fb34d7bcb602"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bceb7dcf-a199-4d67-a45d-6c00e2032109",
    "visible": true
}