surf_3d = noone;

//w = room_width;
//h = room_height;
w = 1024;
h = 768;

direction = 270;
zdirection = 35;
x = 0;
y = 0;
z = 512;
ratio = w / h;
fov = 45;

// Textures
layer0 = background_get_texture(bg_floor);
layer1 = background_get_texture(bg_wall);

