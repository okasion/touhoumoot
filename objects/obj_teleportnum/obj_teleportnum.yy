{
    "id": "592be503-3db4-4fbc-9ec5-7ba4e1077fb8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_teleportnum",
    "eventList": [
        {
            "id": "795a0ca7-83ac-457b-baeb-614d5839e98c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "592be503-3db4-4fbc-9ec5-7ba4e1077fb8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a4b368bf-d51c-4a4f-9e62-cc3e1d18c7cb",
    "visible": true
}