{
    "id": "a3246e9f-f47b-4321-8e1a-a8ec652fc6ff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_boss",
    "eventList": [
        {
            "id": "f42b9a82-4d2e-4128-b741-1b09223fdf07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a3246e9f-f47b-4321-8e1a-a8ec652fc6ff"
        },
        {
            "id": "8647e671-0fa4-49b5-9dce-a104ee50ae54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a3246e9f-f47b-4321-8e1a-a8ec652fc6ff"
        },
        {
            "id": "1376fa7a-137b-4596-88f7-de7989dfbe19",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c6f33aa0-f06a-4915-9835-c292b3d700f3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a3246e9f-f47b-4321-8e1a-a8ec652fc6ff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "04ef50dd-9464-4cc4-b7f0-4421bba4833c",
    "visible": true
}