{
    "id": "43df6e70-7188-4104-8e4c-e6b7a7f41942",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_shield",
    "eventList": [
        {
            "id": "100c64c0-5403-4609-8cfc-a506da568e82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "43df6e70-7188-4104-8e4c-e6b7a7f41942"
        },
        {
            "id": "ed24aa58-ab67-43aa-9b98-ae2d9866246a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "43df6e70-7188-4104-8e4c-e6b7a7f41942"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fb43a399-312a-4f2b-a075-b55aa36bad21",
    "visible": true
}