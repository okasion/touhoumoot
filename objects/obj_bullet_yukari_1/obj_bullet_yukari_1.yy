{
    "id": "a2fa9f18-1837-4e33-9b76-b89804902214",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet_yukari_1",
    "eventList": [
        {
            "id": "9cb61245-19d2-4aed-a2eb-5dffa3a2990a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a2fa9f18-1837-4e33-9b76-b89804902214"
        },
        {
            "id": "cd2a2960-e0e8-4aa8-9d79-958b44b9a9dd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c6f33aa0-f06a-4915-9835-c292b3d700f3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "a2fa9f18-1837-4e33-9b76-b89804902214"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7277da64-2fd5-4041-8458-eae431902df6",
    "visible": true
}