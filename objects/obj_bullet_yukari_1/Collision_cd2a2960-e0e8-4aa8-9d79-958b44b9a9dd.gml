
//game over
image_speed = 0.1;
obj_reimu.sprite_index = spr_reimudie;

//black
draw_set_color(c_black);

//Setup the first frame again
image_index = 0;

audio_play_sound(snd_explosion,50,false);
instance_create_depth(0,0,-9999,obj_fade);
audio_stop_sound(snd_room0);
room_goto(0);