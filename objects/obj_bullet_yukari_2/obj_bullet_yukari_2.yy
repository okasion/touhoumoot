{
    "id": "f402d4c6-26ff-4044-bbb7-1bea4d671d0e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet_yukari_2",
    "eventList": [
        {
            "id": "35da1cb7-fddf-4f17-8b37-a5c5bd81207a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f402d4c6-26ff-4044-bbb7-1bea4d671d0e"
        },
        {
            "id": "bb6bf90e-189c-4817-ac63-259dc159b3be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c6f33aa0-f06a-4915-9835-c292b3d700f3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f402d4c6-26ff-4044-bbb7-1bea4d671d0e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7277da64-2fd5-4041-8458-eae431902df6",
    "visible": true
}