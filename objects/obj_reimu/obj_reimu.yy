{
    "id": "c6f33aa0-f06a-4915-9835-c292b3d700f3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_reimu",
    "eventList": [
        {
            "id": "099ccae5-22a6-427a-b19b-db8c4dc80d87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c6f33aa0-f06a-4915-9835-c292b3d700f3"
        },
        {
            "id": "942227e6-3206-47dd-a8d7-4ba6a0f3ddec",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c6f33aa0-f06a-4915-9835-c292b3d700f3"
        },
        {
            "id": "7a16b9e9-c5dd-447e-ac96-fa2d38c7a79e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ebfb6c13-ae7a-4fe1-afa2-8be0e9ea438b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c6f33aa0-f06a-4915-9835-c292b3d700f3"
        },
        {
            "id": "45337316-82db-4d76-bcec-77931b84c743",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "c6f33aa0-f06a-4915-9835-c292b3d700f3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9257ef31-d274-4b37-ac05-1ae0bbccbc12",
    "visible": true
}