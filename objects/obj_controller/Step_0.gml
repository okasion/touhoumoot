//Yukari appear coming down slowly from top of screen
if (obj_boss.y < 400)
	{
		obj_boss.y += 1;
		obj_bossGhost.y += 1;
	}

//create instance shoot bullets
for (i = 20;i < 400;i += 20){
//if !(instance_exists(obj_bullet_yukari_1)) && (obj_boss.y = i) {
if (obj_boss.y = i) {
	instance_create_layer(obj_boss.x,obj_boss.y,"bullet_layer",obj_bullet_yukari_1);
	}	
}

for (h = 20;h < 400;h += 20){
//if !(instance_exists(obj_bullet_yukari_1)) && (obj_boss.y = i) {
if (obj_boss.y = h) {
	instance_create_layer(obj_boss.x,obj_boss.y,"bullet_layer",obj_bullet_yukari_2);
	}
}


//Yukari "ghost" instances creation
if (yukari_instances < 6){	
	yukari_instances++;	
	instance_create_layer(x + random_range(50,50),y + random_range(50,50),"Instances",obj_bossGhost);
	}

//Yukari backpedals
if (obj_boss.y > 350) && (obj_boss.y > 150)
	{
		obj_boss.y = obj_boss.y -2;
		obj_bossGhost.y = obj_bossGhost.y -2;
	}	
	
//Set Yukari health levels based on an image_index
if (health = 190){
	obj_heat_bar.image_index = 1;
	//Create another dependable health bar outside of screen
	instance_create_layer(-99,56,"Instances",obj_heat_bar_shot);  	
}
if (health = 180){	
	obj_heat_bar.image_index = 2;	
	instance_create_layer(234,-36,"Instances",obj_meteor);	
}
if (health = 170){
	obj_heat_bar.image_index = 3;
}
if (health = 160){
	obj_heat_bar.image_index = 4;
}
if (health = 150){
	obj_heat_bar.image_index = 5;
}
if (health = 140){
	obj_heat_bar.image_index = 6;
}
if (health = 130){
	obj_heat_bar.image_index = 7;
}
if (health = 120){
	obj_heat_bar.image_index = 8;
}
if (health = 110){
	obj_heat_bar.image_index = 9;
}
if (health = 100){
	obj_heat_bar.image_index = 10;
	instance_create_layer(479,5,"Instances",obj_meteor);
	instance_create_layer(obj_boss.x, obj_boss.y,"Instances",obj_shield);
	health = 190;
	obj_heat_bar.image_index = 1;	
}
if (health = 90){
	obj_heat_bar.image_index = 11;
}
if (health = 80){
	obj_heat_bar.image_index = 12;
}
if (health = 70){
	obj_heat_bar.image_index = 13;
}
if (health = 60){
	obj_heat_bar.image_index = 14;
}
if (health = 50){
	obj_heat_bar.image_index = 15;
}
if (health = 40){
	obj_heat_bar.image_index = 16;
}
if (health = 30){
	obj_heat_bar.image_index = 17;
}
if (health = 20){
	obj_heat_bar.image_index = 18;
}
if (health = 10){
	obj_heat_bar.image_index = 19;
}
if (health < 5){
	obj_heat_bar.image_index = 20;
}