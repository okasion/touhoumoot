{
    "id": "c7f65e6d-b47f-48d6-aef6-080e5e6a7cef",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet1",
    "eventList": [
        {
            "id": "4dcca0d2-4bb9-4557-931d-2d328abd9590",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c7f65e6d-b47f-48d6-aef6-080e5e6a7cef"
        },
        {
            "id": "b3715023-10ed-4001-b50c-eb28fc4ab265",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c7f65e6d-b47f-48d6-aef6-080e5e6a7cef"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aca49257-cf03-42b9-8fae-333d3fb514b4",
    "visible": true
}