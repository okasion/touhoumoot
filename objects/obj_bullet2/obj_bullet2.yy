{
    "id": "bf7a63f7-b253-4521-859f-9bc0bdf396e8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet2",
    "eventList": [
        {
            "id": "5556e962-6f84-4232-871c-6fddb03c5c05",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bf7a63f7-b253-4521-859f-9bc0bdf396e8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "aca49257-cf03-42b9-8fae-333d3fb514b4",
    "visible": true
}