{
    "id": "60bed7f2-ade3-4ce8-82cd-8958e5fa3c50",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_menuselect",
    "eventList": [
        {
            "id": "6dbe14fa-4960-4420-83cf-8982457d5382",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "60bed7f2-ade3-4ce8-82cd-8958e5fa3c50"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "590fd499-7f39-490f-821e-6a3efbc06ef1",
    "visible": true
}