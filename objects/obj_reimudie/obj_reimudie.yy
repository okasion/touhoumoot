{
    "id": "706d1fd6-0a7b-4eaf-a327-001f5f3b95c7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_reimudie",
    "eventList": [
        {
            "id": "e0b19291-68b7-494c-ad51-72eaa8d04a78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "706d1fd6-0a7b-4eaf-a327-001f5f3b95c7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3a1d1f6f-b25e-4913-8792-7259d03bfa43",
    "visible": true
}