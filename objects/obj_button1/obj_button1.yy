{
    "id": "7d848bfd-5e54-49fb-8be2-6e0b163cf219",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button1",
    "eventList": [
        {
            "id": "4925579b-f0e6-4963-bd17-0afcc32e4333",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "7d848bfd-5e54-49fb-8be2-6e0b163cf219"
        },
        {
            "id": "d21ce20d-b360-4957-89b3-5c6f242466e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "7d848bfd-5e54-49fb-8be2-6e0b163cf219"
        },
        {
            "id": "3cf9fbb9-5562-47c7-9b75-632d32a6e4e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "7d848bfd-5e54-49fb-8be2-6e0b163cf219"
        },
        {
            "id": "4509a230-657a-4405-a151-7663c699d0e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "7d848bfd-5e54-49fb-8be2-6e0b163cf219"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d276aed-e480-424a-aa86-9a67c480d7f9",
    "visible": true
}