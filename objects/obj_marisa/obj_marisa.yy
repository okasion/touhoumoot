{
    "id": "092519ac-8c9c-40e0-977b-2d3ca65b6459",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_marisa",
    "eventList": [
        {
            "id": "b793f465-2671-40bb-80c0-1755e9f516d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "092519ac-8c9c-40e0-977b-2d3ca65b6459"
        },
        {
            "id": "5cddc87e-7bc8-4596-abc9-f90da263931e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "092519ac-8c9c-40e0-977b-2d3ca65b6459"
        },
        {
            "id": "f95ebf3e-7328-432b-ab44-8ee7f8b7188e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ebfb6c13-ae7a-4fe1-afa2-8be0e9ea438b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "092519ac-8c9c-40e0-977b-2d3ca65b6459"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7308e9d6-4bc0-440e-b75f-5cf48b859496",
    "visible": true
}