//moving around
window_set_caption("Touhoumoot prototype project v0.05");

if  (keyboard_check(vk_right) or keyboard_check(ord("D"))){
	//obj_player.sprite_index = 3;
	if (!place_meeting(x + 2, y, obj_boss)){x =  x + 4;}
	}
else if  (keyboard_check(vk_left) or keyboard_check(ord("A"))){
	//obj_player.sprite_index = 2;
	if (!place_meeting(x - 2,y, obj_boss)){x = x - 4;}
	}
	
else
{
	obj_marisa.sprite_index = 21;
}
if  (keyboard_check(vk_up) or keyboard_check(ord("W"))){
	if (!place_meeting(x,y - 2, obj_boss)) {y = y - 4;}
}
if  (keyboard_check(vk_down) or keyboard_check(ord("S"))){
	if (!place_meeting(x,y + 2, obj_boss)) {y = y + 4;} 
}

image_angle = point_direction(x,y,mouse_x,mouse_y);

//shoot
if (mouse_check_button(mb_left) && (cooldown < 1)){
	instance_create_layer(x,y,"bullet_layer",obj_bullet);
	switch (obj_heat_bar.image_index){
		case 0:
			cooldown = 14;
			break;
		case 1:
			cooldown = 13;
			break;
		case 2:
			cooldown = 12;
			break;
		case 3:
			cooldown = 11;
			break;
		case 4:
			cooldown = 10;
			break;
		case 5:
			cooldown = 9;
			break;
		case 6:
			cooldown = 8;
			break;
		case 7:
			cooldown = 7;
			break;
		case 8:
			cooldown = 6;
			break;
		case 9:
			cooldown = 5;
			break;
		case 10:
			cooldown = 4;
			break;
		case 11:
			cooldown = 3;
			break;
	}
}
cooldown = cooldown -1;
move_wrap(1,1,sprite_width/2);

//change cursor
window_set_cursor(cr_none);
cursor_sprite = spr_cursor;