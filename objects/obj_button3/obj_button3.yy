{
    "id": "0b9f13d8-9d59-4aca-9246-b91e895c8670",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button3",
    "eventList": [
        {
            "id": "92d9a2d5-7066-4612-a586-235570ef2dd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "0b9f13d8-9d59-4aca-9246-b91e895c8670"
        },
        {
            "id": "0dff99e4-9460-4aa4-8dd6-e2271a5575fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "0b9f13d8-9d59-4aca-9246-b91e895c8670"
        },
        {
            "id": "35dd54d7-354b-4021-a6cd-51187df2cea2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0b9f13d8-9d59-4aca-9246-b91e895c8670"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d276aed-e480-424a-aa86-9a67c480d7f9",
    "visible": true
}