instance_destroy();
with (other) {
	instance_destroy();
	var destroyed = instance_create_layer(x,y,"bullet_layer",obj_meteor);
	destroyed.sprite_index = spr_muzzleFlash;
	
}