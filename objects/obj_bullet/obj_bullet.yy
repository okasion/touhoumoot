{
    "id": "4ba31b3e-27ad-4ab6-b6dc-26214b7ea1f3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "22dc4b22-800b-4aa1-bc26-216159b203af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4ba31b3e-27ad-4ab6-b6dc-26214b7ea1f3"
        },
        {
            "id": "f5cd4952-54ae-4f93-97f1-fa5444197e2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "ebfb6c13-ae7a-4fe1-afa2-8be0e9ea438b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4ba31b3e-27ad-4ab6-b6dc-26214b7ea1f3"
        },
        {
            "id": "7f473b66-8d4f-4565-aa78-96c58a70ee7d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a3246e9f-f47b-4321-8e1a-a8ec652fc6ff",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4ba31b3e-27ad-4ab6-b6dc-26214b7ea1f3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f66d321c-2e5d-4091-b1e6-a32c6eba4d6a",
    "visible": true
}