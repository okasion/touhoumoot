{
    "id": "ebfb6c13-ae7a-4fe1-afa2-8be0e9ea438b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_meteor",
    "eventList": [
        {
            "id": "5dbd28d7-ced2-4709-b1ad-8f92f819ea49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ebfb6c13-ae7a-4fe1-afa2-8be0e9ea438b"
        },
        {
            "id": "681c4fa9-ea30-4059-9ba6-1283cdc40950",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ebfb6c13-ae7a-4fe1-afa2-8be0e9ea438b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "787c00f9-360e-47d8-bbc7-d7fe0acd09dc",
    "visible": true
}