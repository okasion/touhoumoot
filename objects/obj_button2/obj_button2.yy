{
    "id": "597f22f1-c000-450d-8760-d5ccd6ad98eb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button2",
    "eventList": [
        {
            "id": "2db15929-906a-42ae-94f5-1827279635f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "597f22f1-c000-450d-8760-d5ccd6ad98eb"
        },
        {
            "id": "b5f4777d-e24f-46a3-be5c-d2d1a4c5fcf5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "597f22f1-c000-450d-8760-d5ccd6ad98eb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d276aed-e480-424a-aa86-9a67c480d7f9",
    "visible": true
}