{
    "id": "8a90fe04-514f-42d9-abf3-6f2b3fdbef9a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fade",
    "eventList": [
        {
            "id": "632b1ba3-1103-4379-8b0d-f8d0b4cf46db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a90fe04-514f-42d9-abf3-6f2b3fdbef9a"
        },
        {
            "id": "696fd321-4d7b-4e9c-b38f-1a388ffde2c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8a90fe04-514f-42d9-abf3-6f2b3fdbef9a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "140b3e29-e17a-432e-a71a-a4663ff8a15f",
    "visible": true
}