{
    "id": "b9d7d889-a1b9-44f7-810e-975d03b813ec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bossGhost",
    "eventList": [
        {
            "id": "717838ce-e482-47c6-8316-cb208d2785d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b9d7d889-a1b9-44f7-810e-975d03b813ec"
        },
        {
            "id": "783f90a1-9802-4414-bf78-499d60e61eca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b9d7d889-a1b9-44f7-810e-975d03b813ec"
        },
        {
            "id": "7989fe96-1eb2-45e1-80a7-8f2cca143736",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c6f33aa0-f06a-4915-9835-c292b3d700f3",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b9d7d889-a1b9-44f7-810e-975d03b813ec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "04ef50dd-9464-4cc4-b7f0-4421bba4833c",
    "visible": true
}