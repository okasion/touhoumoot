{
    "id": "fe6761cb-0af8-45c6-8173-070d07db8ca9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullets_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cd986a12-3401-4014-8400-ec5c83a50969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe6761cb-0af8-45c6-8173-070d07db8ca9",
            "compositeImage": {
                "id": "0d5355f7-a05b-403f-b2e2-7f595cda5488",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd986a12-3401-4014-8400-ec5c83a50969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dadaf424-f6b7-46de-b8ab-cad8e275f7ff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd986a12-3401-4014-8400-ec5c83a50969",
                    "LayerId": "7a6958de-b0f0-4f0d-9fbd-e37e304919f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "7a6958de-b0f0-4f0d-9fbd-e37e304919f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe6761cb-0af8-45c6-8173-070d07db8ca9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 8
}