{
    "id": "f0a35c89-65ff-464f-951a-e40b6aec1a6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_muzzleFlash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 4,
    "bbox_right": 26,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "201860d2-52d4-4dd0-8840-1b221432c5e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0a35c89-65ff-464f-951a-e40b6aec1a6a",
            "compositeImage": {
                "id": "8844704c-e6cb-4e2b-87d4-9d0756c9dd24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "201860d2-52d4-4dd0-8840-1b221432c5e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7c26db4-08cf-4880-8ff3-dafcef270ad4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "201860d2-52d4-4dd0-8840-1b221432c5e6",
                    "LayerId": "4cdbf86c-e43a-476c-85fb-9d1b7863a0aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "4cdbf86c-e43a-476c-85fb-9d1b7863a0aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0a35c89-65ff-464f-951a-e40b6aec1a6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 18
}