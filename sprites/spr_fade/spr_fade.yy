{
    "id": "140b3e29-e17a-432e-a71a-a4663ff8a15f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fade",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6cf78afb-b24d-48f8-9d97-f309ca001ce0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "140b3e29-e17a-432e-a71a-a4663ff8a15f",
            "compositeImage": {
                "id": "0e5a1719-c531-4fde-bcc1-19a90936a2c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6cf78afb-b24d-48f8-9d97-f309ca001ce0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d9ac238-0c68-45f4-9659-055e0f8586cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6cf78afb-b24d-48f8-9d97-f309ca001ce0",
                    "LayerId": "c987402f-7470-4667-8f9e-01aa0bb534b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c987402f-7470-4667-8f9e-01aa0bb534b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "140b3e29-e17a-432e-a71a-a4663ff8a15f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}