{
    "id": "ff5cd0f2-652d-4f63-929b-3361818c7ca0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yukari1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "28621d32-1144-4d11-92ec-6598b4dc2cf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ff5cd0f2-652d-4f63-929b-3361818c7ca0",
            "compositeImage": {
                "id": "1eae321f-4876-4395-83e1-e95849a3ce09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28621d32-1144-4d11-92ec-6598b4dc2cf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ff0e373-4706-4e25-8f9d-f61132d582b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28621d32-1144-4d11-92ec-6598b4dc2cf2",
                    "LayerId": "14d52632-e9f6-4a30-90cb-9764d413bf16"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "14d52632-e9f6-4a30-90cb-9764d413bf16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ff5cd0f2-652d-4f63-929b-3361818c7ca0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": 19,
    "yorig": 34
}