{
    "id": "590fd499-7f39-490f-821e-6a3efbc06ef1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menuselect",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 267,
    "bbox_left": 0,
    "bbox_right": 280,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87aff02d-0e0f-4d62-afb3-546a7c247762",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "590fd499-7f39-490f-821e-6a3efbc06ef1",
            "compositeImage": {
                "id": "224c6fbb-31ad-4acc-aef3-141ca4bac893",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87aff02d-0e0f-4d62-afb3-546a7c247762",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "417bcb4d-0487-4222-9513-0d653c640f56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87aff02d-0e0f-4d62-afb3-546a7c247762",
                    "LayerId": "44380c32-9def-49dc-9273-fa58f7a780e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 268,
    "layers": [
        {
            "id": "44380c32-9def-49dc-9273-fa58f7a780e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "590fd499-7f39-490f-821e-6a3efbc06ef1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 281,
    "xorig": 140,
    "yorig": 134
}