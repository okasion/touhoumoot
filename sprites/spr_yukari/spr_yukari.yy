{
    "id": "04ef50dd-9464-4cc4-b7f0-4421bba4833c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yukari",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7e6751cf-a7ae-4f46-9bda-278cead1a763",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04ef50dd-9464-4cc4-b7f0-4421bba4833c",
            "compositeImage": {
                "id": "6e8fc4cc-4d50-4950-a1f4-a040dfc0010e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e6751cf-a7ae-4f46-9bda-278cead1a763",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf838c53-9fd1-48cf-a463-64ec4a9937ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e6751cf-a7ae-4f46-9bda-278cead1a763",
                    "LayerId": "b8d2ca4a-66ab-4d97-9c75-4f6f6ab12f94"
                }
            ]
        },
        {
            "id": "2378129d-8c29-4230-b32a-6e3c3d3c36b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04ef50dd-9464-4cc4-b7f0-4421bba4833c",
            "compositeImage": {
                "id": "a352eda9-44b1-4892-a5ab-6c29c9b36440",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2378129d-8c29-4230-b32a-6e3c3d3c36b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3c20ce8-dde4-4840-add6-02dac65d8701",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2378129d-8c29-4230-b32a-6e3c3d3c36b3",
                    "LayerId": "b8d2ca4a-66ab-4d97-9c75-4f6f6ab12f94"
                }
            ]
        },
        {
            "id": "5d6e54be-8b6d-42e3-bd36-a29e9ec6b09d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04ef50dd-9464-4cc4-b7f0-4421bba4833c",
            "compositeImage": {
                "id": "559eb81e-86cd-4518-b941-03cfb6ab74a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d6e54be-8b6d-42e3-bd36-a29e9ec6b09d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47a2e596-7650-4110-b583-75483e5e86fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d6e54be-8b6d-42e3-bd36-a29e9ec6b09d",
                    "LayerId": "b8d2ca4a-66ab-4d97-9c75-4f6f6ab12f94"
                }
            ]
        },
        {
            "id": "1f07ff7c-3d4a-45ac-a4f6-9b442ba893b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "04ef50dd-9464-4cc4-b7f0-4421bba4833c",
            "compositeImage": {
                "id": "04511d05-f8c2-4ece-a4c9-6202cdae266a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f07ff7c-3d4a-45ac-a4f6-9b442ba893b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33dc2702-ca36-4c2a-9692-cabbd943a4b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f07ff7c-3d4a-45ac-a4f6-9b442ba893b7",
                    "LayerId": "b8d2ca4a-66ab-4d97-9c75-4f6f6ab12f94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 71,
    "layers": [
        {
            "id": "b8d2ca4a-66ab-4d97-9c75-4f6f6ab12f94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "04ef50dd-9464-4cc4-b7f0-4421bba4833c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": 19,
    "yorig": 35
}