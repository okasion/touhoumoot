{
    "id": "e7123683-284e-4f51-b4c8-717c479f22bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullets_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58520541-e640-41c8-b66a-3742e8280a9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7123683-284e-4f51-b4c8-717c479f22bb",
            "compositeImage": {
                "id": "7d97d2cd-6d2f-4038-a425-7dbc7edf6256",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58520541-e640-41c8-b66a-3742e8280a9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "638c16e4-67b4-4cae-8107-0781ce38199e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58520541-e640-41c8-b66a-3742e8280a9e",
                    "LayerId": "97359cd6-5644-43f6-8604-1ec211c8d2b8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "97359cd6-5644-43f6-8604-1ec211c8d2b8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7123683-284e-4f51-b4c8-717c479f22bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 6
}