{
    "id": "e5e93bf9-3ac2-4362-99c1-37edcb6dad3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_heat_bar_shot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6945d262-3040-4781-b48e-1aa17823717a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e5e93bf9-3ac2-4362-99c1-37edcb6dad3e",
            "compositeImage": {
                "id": "01ed5867-8df0-4b31-9fd5-02f43a33bd08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6945d262-3040-4781-b48e-1aa17823717a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e236e019-4716-41cc-8be2-d192779a3576",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6945d262-3040-4781-b48e-1aa17823717a",
                    "LayerId": "12d35da9-cbe1-46e2-ab30-8b0cca4975e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "12d35da9-cbe1-46e2-ab30-8b0cca4975e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e5e93bf9-3ac2-4362-99c1-37edcb6dad3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 41
}