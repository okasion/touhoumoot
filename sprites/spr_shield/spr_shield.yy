{
    "id": "fb43a399-312a-4f2b-a075-b55aa36bad21",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shield",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 9,
    "bbox_right": 118,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a14084fc-b44f-40d0-b70b-cd0ca23b7ef8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb43a399-312a-4f2b-a075-b55aa36bad21",
            "compositeImage": {
                "id": "0b721ca7-761d-4398-8c98-221a155fc4fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a14084fc-b44f-40d0-b70b-cd0ca23b7ef8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fae9be79-e0f3-419a-91f0-17b70badb145",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a14084fc-b44f-40d0-b70b-cd0ca23b7ef8",
                    "LayerId": "f2f506a7-b8e7-4491-beec-fd274bfb4f02"
                }
            ]
        },
        {
            "id": "d5db23c1-1962-4520-a4e2-2989f9f2eefd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb43a399-312a-4f2b-a075-b55aa36bad21",
            "compositeImage": {
                "id": "b053360c-fdfd-4229-aee2-97af7ce0cd8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5db23c1-1962-4520-a4e2-2989f9f2eefd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1b54cd5-60c0-40d5-9085-cf8ec3fd278f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5db23c1-1962-4520-a4e2-2989f9f2eefd",
                    "LayerId": "f2f506a7-b8e7-4491-beec-fd274bfb4f02"
                }
            ]
        },
        {
            "id": "d7369f24-60cd-4dab-951e-5dbee14c2ddc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb43a399-312a-4f2b-a075-b55aa36bad21",
            "compositeImage": {
                "id": "e910ad65-0909-4ed2-bf80-5e6998dc7cb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7369f24-60cd-4dab-951e-5dbee14c2ddc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40b8e06d-f38d-492f-84d0-d2ba13677df7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7369f24-60cd-4dab-951e-5dbee14c2ddc",
                    "LayerId": "f2f506a7-b8e7-4491-beec-fd274bfb4f02"
                }
            ]
        },
        {
            "id": "1a516851-a597-48d9-817a-083292baad61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb43a399-312a-4f2b-a075-b55aa36bad21",
            "compositeImage": {
                "id": "fb051832-2854-42a6-a19c-871403c7e58a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a516851-a597-48d9-817a-083292baad61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cba3eca-018d-49e9-bccb-b2a9595ae44e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a516851-a597-48d9-817a-083292baad61",
                    "LayerId": "f2f506a7-b8e7-4491-beec-fd274bfb4f02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "f2f506a7-b8e7-4491-beec-fd274bfb4f02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb43a399-312a-4f2b-a075-b55aa36bad21",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}