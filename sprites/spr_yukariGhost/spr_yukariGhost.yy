{
    "id": "5c7ecd0d-7090-4dda-8064-6663a0802020",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yukariGhost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 0,
    "bbox_right": 37,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 3,
    "coltolerance": 45,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7419ff9-1557-4f1f-8891-8eb0dfeb9c47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c7ecd0d-7090-4dda-8064-6663a0802020",
            "compositeImage": {
                "id": "03e3a64d-d169-4030-b68e-4f32a74e8c48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7419ff9-1557-4f1f-8891-8eb0dfeb9c47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ce82965-7d0e-464e-b8c8-46e91d414f75",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7419ff9-1557-4f1f-8891-8eb0dfeb9c47",
                    "LayerId": "45e1f28e-c123-4a94-9ec3-2c1b5a9117a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "45e1f28e-c123-4a94-9ec3-2c1b5a9117a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c7ecd0d-7090-4dda-8064-6663a0802020",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 2,
    "playbackSpeedType": 0,
    "premultiplyAlpha": true,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 38,
    "xorig": 19,
    "yorig": 34
}