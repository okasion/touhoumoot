{
    "id": "7308e9d6-4bc0-440e-b75f-5cf48b859496",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marisa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 43,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "111d8f86-8669-4762-b063-334e531f5caa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7308e9d6-4bc0-440e-b75f-5cf48b859496",
            "compositeImage": {
                "id": "8ec01652-e237-456f-8433-c4b23d70e285",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "111d8f86-8669-4762-b063-334e531f5caa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86244a04-4e1c-439d-8349-42c2cf0d607d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "111d8f86-8669-4762-b063-334e531f5caa",
                    "LayerId": "a848b81a-59f7-49b5-a7c5-c8bc96429d3b"
                }
            ]
        },
        {
            "id": "f91e0628-596e-42a3-ac1d-9676c1477db4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7308e9d6-4bc0-440e-b75f-5cf48b859496",
            "compositeImage": {
                "id": "2e2f38bb-f48c-4ede-96a1-a054d7d327a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f91e0628-596e-42a3-ac1d-9676c1477db4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c8093fb-49d0-47e9-acae-7f895795a1ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f91e0628-596e-42a3-ac1d-9676c1477db4",
                    "LayerId": "a848b81a-59f7-49b5-a7c5-c8bc96429d3b"
                }
            ]
        },
        {
            "id": "bbce1d57-aa0a-49f2-b2f1-e3c541f29ba9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7308e9d6-4bc0-440e-b75f-5cf48b859496",
            "compositeImage": {
                "id": "3cdfa1ea-753e-44a8-a6cb-2f3b33c11b66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbce1d57-aa0a-49f2-b2f1-e3c541f29ba9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2739335b-fd45-4889-bc97-00ff3b1dfaea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbce1d57-aa0a-49f2-b2f1-e3c541f29ba9",
                    "LayerId": "a848b81a-59f7-49b5-a7c5-c8bc96429d3b"
                }
            ]
        },
        {
            "id": "be471732-b622-45b0-9181-01f0221cdd7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7308e9d6-4bc0-440e-b75f-5cf48b859496",
            "compositeImage": {
                "id": "c45c7d2d-a8bf-4e02-8dde-7ac1dc118c67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be471732-b622-45b0-9181-01f0221cdd7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8906dd73-eb68-46dc-a331-e93bda7570b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be471732-b622-45b0-9181-01f0221cdd7c",
                    "LayerId": "a848b81a-59f7-49b5-a7c5-c8bc96429d3b"
                }
            ]
        },
        {
            "id": "2bee42f3-2d00-42f0-8d15-80601b776be8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7308e9d6-4bc0-440e-b75f-5cf48b859496",
            "compositeImage": {
                "id": "00cfe3ef-12f5-4264-9b64-a9de5d78e7b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bee42f3-2d00-42f0-8d15-80601b776be8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a76c2c66-8702-4673-95cb-91c674c08d08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bee42f3-2d00-42f0-8d15-80601b776be8",
                    "LayerId": "a848b81a-59f7-49b5-a7c5-c8bc96429d3b"
                }
            ]
        },
        {
            "id": "7d534a3c-fb56-4d87-b5b0-1c91bff8a747",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7308e9d6-4bc0-440e-b75f-5cf48b859496",
            "compositeImage": {
                "id": "7244a232-885e-4051-b5b2-745bed83d4c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d534a3c-fb56-4d87-b5b0-1c91bff8a747",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1557c1b5-4694-418b-9839-0aff13675e62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d534a3c-fb56-4d87-b5b0-1c91bff8a747",
                    "LayerId": "a848b81a-59f7-49b5-a7c5-c8bc96429d3b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "a848b81a-59f7-49b5-a7c5-c8bc96429d3b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7308e9d6-4bc0-440e-b75f-5cf48b859496",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 22,
    "yorig": 14
}