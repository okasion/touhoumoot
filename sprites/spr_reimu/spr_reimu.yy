{
    "id": "9257ef31-d274-4b37-ac05-1ae0bbccbc12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_reimu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 45,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23930841-ed2a-41f3-8cc0-ec6aa22314d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9257ef31-d274-4b37-ac05-1ae0bbccbc12",
            "compositeImage": {
                "id": "d7c9e0ae-0641-4af1-a1d6-63a1086791ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23930841-ed2a-41f3-8cc0-ec6aa22314d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9254431e-b244-4023-959c-2154a52d16b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23930841-ed2a-41f3-8cc0-ec6aa22314d4",
                    "LayerId": "e5213e36-7951-45bc-8191-3fc1e2929073"
                }
            ]
        },
        {
            "id": "9b879366-c85e-4f16-9e0c-b91e1585d091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9257ef31-d274-4b37-ac05-1ae0bbccbc12",
            "compositeImage": {
                "id": "9979fda3-baed-42b1-9b55-e08dda5458ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b879366-c85e-4f16-9e0c-b91e1585d091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3bbf7e3-c671-4e9b-bfa6-d3cdb38369db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b879366-c85e-4f16-9e0c-b91e1585d091",
                    "LayerId": "e5213e36-7951-45bc-8191-3fc1e2929073"
                }
            ]
        },
        {
            "id": "7c7a906e-ea9e-4811-be02-98fb352b52a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9257ef31-d274-4b37-ac05-1ae0bbccbc12",
            "compositeImage": {
                "id": "d3a956da-5bb8-4862-a603-abb8f9ceae66",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c7a906e-ea9e-4811-be02-98fb352b52a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d00cd4c-d4eb-4408-b9bd-462877ea7f92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c7a906e-ea9e-4811-be02-98fb352b52a7",
                    "LayerId": "e5213e36-7951-45bc-8191-3fc1e2929073"
                }
            ]
        },
        {
            "id": "6fba7afd-6673-4da8-b95e-c7e688732912",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9257ef31-d274-4b37-ac05-1ae0bbccbc12",
            "compositeImage": {
                "id": "6c4b0f7f-fc1a-4f54-a61e-556fcd0619e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6fba7afd-6673-4da8-b95e-c7e688732912",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a679dd4-e237-4bbb-b2b0-667bdc546a47",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6fba7afd-6673-4da8-b95e-c7e688732912",
                    "LayerId": "e5213e36-7951-45bc-8191-3fc1e2929073"
                }
            ]
        },
        {
            "id": "54d70809-2517-40e4-8fd9-ecc118f29b29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9257ef31-d274-4b37-ac05-1ae0bbccbc12",
            "compositeImage": {
                "id": "4790fd5a-7b9e-43a5-bb02-8d21d094b38b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54d70809-2517-40e4-8fd9-ecc118f29b29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "601e4d87-bf59-4a0e-8378-75f9f43c13d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54d70809-2517-40e4-8fd9-ecc118f29b29",
                    "LayerId": "e5213e36-7951-45bc-8191-3fc1e2929073"
                }
            ]
        },
        {
            "id": "220cf1e1-f9b4-4cca-8267-9bb855039024",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9257ef31-d274-4b37-ac05-1ae0bbccbc12",
            "compositeImage": {
                "id": "1795848c-bd3e-4b21-aa21-37f127cee156",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "220cf1e1-f9b4-4cca-8267-9bb855039024",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d67d728-ba5c-4a30-ab3d-9d531eab351f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "220cf1e1-f9b4-4cca-8267-9bb855039024",
                    "LayerId": "e5213e36-7951-45bc-8191-3fc1e2929073"
                }
            ]
        },
        {
            "id": "c76b6d2f-6ac3-4743-8630-4c0d23af8ef9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9257ef31-d274-4b37-ac05-1ae0bbccbc12",
            "compositeImage": {
                "id": "bc4578ea-04c4-4749-aa72-d607b3ff4801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c76b6d2f-6ac3-4743-8630-4c0d23af8ef9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4cb172b-3840-4806-af4a-9c69e80a0a71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c76b6d2f-6ac3-4743-8630-4c0d23af8ef9",
                    "LayerId": "e5213e36-7951-45bc-8191-3fc1e2929073"
                }
            ]
        },
        {
            "id": "67d53d65-a215-44f9-8d09-f6ae7c403039",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9257ef31-d274-4b37-ac05-1ae0bbccbc12",
            "compositeImage": {
                "id": "c827df08-4da1-42f3-b5df-92ef326ace7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67d53d65-a215-44f9-8d09-f6ae7c403039",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37150f4a-948c-4650-b27d-3f4bcda8785f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67d53d65-a215-44f9-8d09-f6ae7c403039",
                    "LayerId": "e5213e36-7951-45bc-8191-3fc1e2929073"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e5213e36-7951-45bc-8191-3fc1e2929073",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9257ef31-d274-4b37-ac05-1ae0bbccbc12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4287050789,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 16
}