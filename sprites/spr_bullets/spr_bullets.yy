{
    "id": "f66d321c-2e5d-4091-b1e6-a32c6eba4d6a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullets",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c2c37f10-41a6-4d0b-a745-8721c2685fcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f66d321c-2e5d-4091-b1e6-a32c6eba4d6a",
            "compositeImage": {
                "id": "23e415fe-de64-4b56-8d98-84040a3945a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2c37f10-41a6-4d0b-a745-8721c2685fcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c5ba27a-8e14-4128-b00b-c615ab1a1de9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2c37f10-41a6-4d0b-a745-8721c2685fcc",
                    "LayerId": "b090de23-0c76-4765-92d3-ee235df91d15"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 24,
    "layers": [
        {
            "id": "b090de23-0c76-4765-92d3-ee235df91d15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f66d321c-2e5d-4091-b1e6-a32c6eba4d6a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 6,
    "yorig": 12
}