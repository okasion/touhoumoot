{
    "id": "4994623b-882c-498e-a8be-ab96dc8e5f09",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_barrier1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2102375-d002-4843-adba-051507ccfac3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4994623b-882c-498e-a8be-ab96dc8e5f09",
            "compositeImage": {
                "id": "c1e3c93b-a6c6-4598-89df-9c2284d51b26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2102375-d002-4843-adba-051507ccfac3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88fb93fb-9b8a-4075-8b93-04671796189a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2102375-d002-4843-adba-051507ccfac3",
                    "LayerId": "5eeba2c5-18ee-4f10-b486-f8b111e406a6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "5eeba2c5-18ee-4f10-b486-f8b111e406a6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4994623b-882c-498e-a8be-ab96dc8e5f09",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}