{
    "id": "1599f9d5-50e9-426c-a318-67ac739af244",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_clouds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1499,
    "bbox_left": 0,
    "bbox_right": 2999,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 159,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40680ab5-399e-4f9f-9b2a-33cb2d4eb82c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1599f9d5-50e9-426c-a318-67ac739af244",
            "compositeImage": {
                "id": "fed5174a-c1b0-4711-92ba-e5e5faa65017",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40680ab5-399e-4f9f-9b2a-33cb2d4eb82c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44c55947-b786-4228-8301-acafcd2456dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40680ab5-399e-4f9f-9b2a-33cb2d4eb82c",
                    "LayerId": "29f807a1-56b5-458b-b2ec-5bf1cc9dc61d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1500,
    "layers": [
        {
            "id": "29f807a1-56b5-458b-b2ec-5bf1cc9dc61d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1599f9d5-50e9-426c-a318-67ac739af244",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3000,
    "xorig": 1500,
    "yorig": 750
}