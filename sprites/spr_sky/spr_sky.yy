{
    "id": "f5c67643-29ce-45a6-9fef-1d622b326ed4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sky",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 23,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "968b46df-ac1e-4b89-b253-78d775075891",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f5c67643-29ce-45a6-9fef-1d622b326ed4",
            "compositeImage": {
                "id": "e924daff-6102-442b-9709-e4fb023d3538",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "968b46df-ac1e-4b89-b253-78d775075891",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08102ba4-b39f-4174-affc-91f5b75834f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "968b46df-ac1e-4b89-b253-78d775075891",
                    "LayerId": "2fc3537a-8d60-41e1-ac85-c8365d826446"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "2fc3537a-8d60-41e1-ac85-c8365d826446",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f5c67643-29ce-45a6-9fef-1d622b326ed4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": true,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}