{
    "id": "58eb5885-6cab-44e7-b96e-396902ab026e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "92504090-05ae-4e08-98ae-f44dcbac9b48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58eb5885-6cab-44e7-b96e-396902ab026e",
            "compositeImage": {
                "id": "34d75efe-4352-4825-a1ea-978b0ac55ae0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92504090-05ae-4e08-98ae-f44dcbac9b48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cec517bd-9702-4b34-96fb-d1c67db13f04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92504090-05ae-4e08-98ae-f44dcbac9b48",
                    "LayerId": "7af5ab7b-7072-4e6e-a59c-5cdb5dc12aad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "7af5ab7b-7072-4e6e-a59c-5cdb5dc12aad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58eb5885-6cab-44e7-b96e-396902ab026e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 512,
    "yorig": 512
}