{
    "id": "a662ff94-d30c-4484-92b8-4c8904cb6476",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_floor2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1279,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f2084ca2-1207-4ff3-a678-6feb96cb8532",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a662ff94-d30c-4484-92b8-4c8904cb6476",
            "compositeImage": {
                "id": "a69f4d4f-e9a8-495a-9bb1-a2e095689c87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f2084ca2-1207-4ff3-a678-6feb96cb8532",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33b38d15-e987-4f0e-9a7c-a1938fcfc821",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f2084ca2-1207-4ff3-a678-6feb96cb8532",
                    "LayerId": "c6d3c288-3fa6-4543-bf2e-827a815e0a45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1280,
    "layers": [
        {
            "id": "c6d3c288-3fa6-4543-bf2e-827a815e0a45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a662ff94-d30c-4484-92b8-4c8904cb6476",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 640,
    "yorig": 640
}