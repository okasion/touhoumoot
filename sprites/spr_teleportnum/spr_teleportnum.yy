{
    "id": "a4b368bf-d51c-4a4f-9e62-cc3e1d18c7cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_teleportnum",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 4,
    "bbox_right": 26,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "24e5eaf4-9eff-43d6-a996-9435a70b2bfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4b368bf-d51c-4a4f-9e62-cc3e1d18c7cb",
            "compositeImage": {
                "id": "9174ed9a-5c1c-4bcc-87b2-0aa76e6145b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24e5eaf4-9eff-43d6-a996-9435a70b2bfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5562d00-02bd-4081-9ec5-118cfaa1287a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24e5eaf4-9eff-43d6-a996-9435a70b2bfc",
                    "LayerId": "0b8b2e3c-744a-47e7-aa00-b4298daea8aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "0b8b2e3c-744a-47e7-aa00-b4298daea8aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4b368bf-d51c-4a4f-9e62-cc3e1d18c7cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 18
}