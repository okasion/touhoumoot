{
    "id": "01db0d48-3327-489f-9e01-174c314457b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gap1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 0,
    "bbox_right": 17,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "988e5077-8776-4e28-bc62-7390f911d3ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "c7896b28-5be5-4002-af6b-0a02f032d4d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "988e5077-8776-4e28-bc62-7390f911d3ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21595e1f-f548-4297-8e78-358bacef9107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "988e5077-8776-4e28-bc62-7390f911d3ff",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "63d32144-0775-423c-8499-d0370ebc4ee0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "74a5970f-6f5f-4494-9c14-09efbafb7395",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63d32144-0775-423c-8499-d0370ebc4ee0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1d8c330-1535-4f70-818d-6ca4667c46f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63d32144-0775-423c-8499-d0370ebc4ee0",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "ee0a4162-5065-40c2-b889-0894111a1cc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "ebfc0303-c3cb-4c89-bd8e-6bf351005835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee0a4162-5065-40c2-b889-0894111a1cc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22e0d5d6-811f-47e3-9072-7ab877895053",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee0a4162-5065-40c2-b889-0894111a1cc7",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "01e26fa3-a032-4502-8b44-3907f23c6c60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "368cb80f-4031-4dde-a1a8-6694f74c8887",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01e26fa3-a032-4502-8b44-3907f23c6c60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "019f5b0e-169a-44e5-9c21-ba32f90c6f0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01e26fa3-a032-4502-8b44-3907f23c6c60",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "048c2e22-c198-457a-b8fd-574dfd5da3c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "813fad16-cc7c-4815-9c9e-1c20c58faebb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "048c2e22-c198-457a-b8fd-574dfd5da3c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "691bc7ff-8b2b-4b6d-9471-4687a6ad9fe1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "048c2e22-c198-457a-b8fd-574dfd5da3c7",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "3ebb9a05-e8ac-4773-8c41-f6665ba3bb7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "3db8c14b-9ef4-4ba4-96e8-4fd5475b8c9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ebb9a05-e8ac-4773-8c41-f6665ba3bb7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77d019c8-72bc-454b-9589-f0f2075ef165",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ebb9a05-e8ac-4773-8c41-f6665ba3bb7c",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "c3b1c547-fe8d-4758-a532-07a305905556",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "cbcbf249-250e-4c96-bece-621cf1eca50a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3b1c547-fe8d-4758-a532-07a305905556",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1448a32-4840-45c0-953f-383a01ee4c0f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3b1c547-fe8d-4758-a532-07a305905556",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "b5af46ca-ba02-4191-93bc-32d83d14f0d5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "ec638699-c29f-42e7-ad5e-779eb86236cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5af46ca-ba02-4191-93bc-32d83d14f0d5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "28285bd2-2823-4687-88ab-ae2b0f318c59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5af46ca-ba02-4191-93bc-32d83d14f0d5",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "3eea911c-f350-4be1-898e-5a20d0015fe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "5a848f3d-0ef0-4c9b-8f9d-376bec71e616",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3eea911c-f350-4be1-898e-5a20d0015fe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca5f7d89-b2a1-4656-b7ea-62586177ef24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3eea911c-f350-4be1-898e-5a20d0015fe5",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "7bc89efa-ac82-49c1-8ed5-3f97492ba607",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "338179cd-9068-455e-887b-9dcc2840d3d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7bc89efa-ac82-49c1-8ed5-3f97492ba607",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae9bb6fc-21c8-449b-b8dc-4244674cbbb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7bc89efa-ac82-49c1-8ed5-3f97492ba607",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "1c0b284b-d1dd-48c2-bed1-ed960e068709",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "8051a160-def4-4238-bed8-5bff9e31375c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c0b284b-d1dd-48c2-bed1-ed960e068709",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "95ec2b0a-429c-48db-8165-e55fd07861d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c0b284b-d1dd-48c2-bed1-ed960e068709",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "7f2c2ada-f685-4087-aeb6-ffe9e8b7d093",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "dc78bee6-8b92-408f-872d-e4d86a13f973",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f2c2ada-f685-4087-aeb6-ffe9e8b7d093",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1000b4ff-3ccb-4d64-b293-c0c645cd75cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f2c2ada-f685-4087-aeb6-ffe9e8b7d093",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "33f1ac05-9f50-4268-a967-e9d0f346d954",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "839ce1f8-c705-4d65-998a-8d3f7e9eaf2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33f1ac05-9f50-4268-a967-e9d0f346d954",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5025fa39-82c3-4eea-af8c-9b109bde1bac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33f1ac05-9f50-4268-a967-e9d0f346d954",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "9a962d7f-67ae-4c9f-86bd-df39bf4b8b32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "81ca0a0e-3f2f-4f40-9aa7-d517beb144cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a962d7f-67ae-4c9f-86bd-df39bf4b8b32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edcc9667-8fc3-4a4a-b3dd-4abd8e173cd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a962d7f-67ae-4c9f-86bd-df39bf4b8b32",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "e0dcbb1a-88fc-421a-a82b-01be8e7483c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "845a0722-706a-4d84-b193-c7ac89f40f8a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0dcbb1a-88fc-421a-a82b-01be8e7483c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3ff8ff6-bce1-4500-90c4-e599f1190c1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0dcbb1a-88fc-421a-a82b-01be8e7483c5",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "bd1867c5-aff5-4c77-95a7-9576e741d743",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "d5e2b2ec-1ec4-4b33-9ea1-97320dc6303a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd1867c5-aff5-4c77-95a7-9576e741d743",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b24768b5-ec63-413b-aa86-fc68e781a450",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd1867c5-aff5-4c77-95a7-9576e741d743",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "36375aff-9478-42ea-b7d7-9a6624d900c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "008f4473-a8c1-41d6-8dd7-653abffeb7ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36375aff-9478-42ea-b7d7-9a6624d900c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20e2d36b-c6d7-404d-b7cb-1c3561e200f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36375aff-9478-42ea-b7d7-9a6624d900c8",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "af905ece-4ef4-4b2d-8820-35ace13fbde4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "bed06d5c-933e-4883-a0f6-d0973d308966",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "af905ece-4ef4-4b2d-8820-35ace13fbde4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51d89bf4-68cf-4cf5-bea7-d78115599a4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "af905ece-4ef4-4b2d-8820-35ace13fbde4",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        },
        {
            "id": "683055d8-4886-4a90-a23b-9b253d1f273e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "compositeImage": {
                "id": "9473463b-6401-402e-b266-487ef026037e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "683055d8-4886-4a90-a23b-9b253d1f273e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "163ebd31-9b7b-4807-a9c0-d8ff45cadda0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "683055d8-4886-4a90-a23b-9b253d1f273e",
                    "LayerId": "71bd9726-63f5-4ec8-8215-d6e186f5ea33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 41,
    "layers": [
        {
            "id": "71bd9726-63f5-4ec8-8215-d6e186f5ea33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "01db0d48-3327-489f-9e01-174c314457b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 20
}