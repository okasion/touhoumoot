{
    "id": "20925690-383c-4950-8078-cdc2fb73749b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerright",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 45,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b967e3f-424e-4801-8a45-860fd03102d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20925690-383c-4950-8078-cdc2fb73749b",
            "compositeImage": {
                "id": "089e27ff-40b7-44fb-b3fc-7bfe62d3ae09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b967e3f-424e-4801-8a45-860fd03102d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15a85766-41c4-472c-a899-60ebeb5f018b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b967e3f-424e-4801-8a45-860fd03102d4",
                    "LayerId": "802acef8-e104-46c9-bb34-283dc765aeae"
                }
            ]
        },
        {
            "id": "be542248-702e-4fb2-bd56-88b3475a8b4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20925690-383c-4950-8078-cdc2fb73749b",
            "compositeImage": {
                "id": "d747f825-5fb7-44b7-ba4a-764872c1e7b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be542248-702e-4fb2-bd56-88b3475a8b4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "36089117-2edf-4843-a282-391c2f1544eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be542248-702e-4fb2-bd56-88b3475a8b4f",
                    "LayerId": "802acef8-e104-46c9-bb34-283dc765aeae"
                }
            ]
        },
        {
            "id": "716f2cc9-6eed-4606-a22f-05cd37f40dbb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20925690-383c-4950-8078-cdc2fb73749b",
            "compositeImage": {
                "id": "5e94ba5a-e184-48d7-92ba-e770a2f67f8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "716f2cc9-6eed-4606-a22f-05cd37f40dbb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ede63c4-d0ef-4ea3-b922-e62fe84847e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "716f2cc9-6eed-4606-a22f-05cd37f40dbb",
                    "LayerId": "802acef8-e104-46c9-bb34-283dc765aeae"
                }
            ]
        },
        {
            "id": "ee9147eb-742f-424c-9b28-8f01aaf75716",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20925690-383c-4950-8078-cdc2fb73749b",
            "compositeImage": {
                "id": "4cec4b3b-c023-4236-b1e4-8f89e1f8b348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee9147eb-742f-424c-9b28-8f01aaf75716",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d35b370-c981-46a8-a05a-3c4be0551c8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee9147eb-742f-424c-9b28-8f01aaf75716",
                    "LayerId": "802acef8-e104-46c9-bb34-283dc765aeae"
                }
            ]
        },
        {
            "id": "ee9624de-b22e-4db0-a7b2-2cf4e8afb3e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20925690-383c-4950-8078-cdc2fb73749b",
            "compositeImage": {
                "id": "8382fbee-1278-43cf-baaf-b13649af5795",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee9624de-b22e-4db0-a7b2-2cf4e8afb3e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc7fe182-5f32-4bbc-b3bd-fdc2586e775a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee9624de-b22e-4db0-a7b2-2cf4e8afb3e5",
                    "LayerId": "802acef8-e104-46c9-bb34-283dc765aeae"
                }
            ]
        },
        {
            "id": "ba7ea169-dbdb-41e9-ab3e-cf71eb49b6d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20925690-383c-4950-8078-cdc2fb73749b",
            "compositeImage": {
                "id": "1afba8be-6b61-4561-a6fd-d63870d26994",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba7ea169-dbdb-41e9-ab3e-cf71eb49b6d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "03fe95df-7298-44d2-b3e3-c07d784d6386",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba7ea169-dbdb-41e9-ab3e-cf71eb49b6d7",
                    "LayerId": "802acef8-e104-46c9-bb34-283dc765aeae"
                }
            ]
        },
        {
            "id": "07266a5a-3591-4c27-9e64-f67ef459545c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20925690-383c-4950-8078-cdc2fb73749b",
            "compositeImage": {
                "id": "3af214d2-25fb-4c42-b82e-592e7910260d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07266a5a-3591-4c27-9e64-f67ef459545c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93801650-2576-40bb-9f3f-a7d1170d27e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07266a5a-3591-4c27-9e64-f67ef459545c",
                    "LayerId": "802acef8-e104-46c9-bb34-283dc765aeae"
                }
            ]
        },
        {
            "id": "6b2797a7-52b6-481b-9460-ccfb8248e256",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "20925690-383c-4950-8078-cdc2fb73749b",
            "compositeImage": {
                "id": "2b68d750-cf64-486e-a59a-6f8677a1b9c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b2797a7-52b6-481b-9460-ccfb8248e256",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa377cb5-73fa-407b-bd97-27dbe99f160d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b2797a7-52b6-481b-9460-ccfb8248e256",
                    "LayerId": "802acef8-e104-46c9-bb34-283dc765aeae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "802acef8-e104-46c9-bb34-283dc765aeae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "20925690-383c-4950-8078-cdc2fb73749b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 16
}