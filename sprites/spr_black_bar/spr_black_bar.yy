{
    "id": "0d407638-1f40-4525-8e65-75cd7b374519",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_black_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ad1c224-6b10-4fb7-8fd4-afe8ff22de9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d407638-1f40-4525-8e65-75cd7b374519",
            "compositeImage": {
                "id": "c5bfeda5-db81-41cb-b090-f871d7f8be4f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ad1c224-6b10-4fb7-8fd4-afe8ff22de9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce2e1ec2-89eb-4558-a5fb-ea4b4fa4fe8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ad1c224-6b10-4fb7-8fd4-afe8ff22de9b",
                    "LayerId": "0a9477b3-a0a6-429b-9cc7-78073cc1c055"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "0a9477b3-a0a6-429b-9cc7-78073cc1c055",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d407638-1f40-4525-8e65-75cd7b374519",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 23
}