{
    "id": "e4c0145a-0ec2-4c1a-bc04-62062e6d60bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menutext",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 76,
    "bbox_left": 52,
    "bbox_right": 629,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b62c7aec-0d79-4737-af8c-8eea0eac6deb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4c0145a-0ec2-4c1a-bc04-62062e6d60bb",
            "compositeImage": {
                "id": "8c3f64fc-8703-4946-9ff4-c4661bcaceb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b62c7aec-0d79-4737-af8c-8eea0eac6deb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f1acea3-67a9-493d-a77e-ed4b1757a900",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b62c7aec-0d79-4737-af8c-8eea0eac6deb",
                    "LayerId": "25cad031-f20e-4ce2-8f75-578bf00034c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 77,
    "layers": [
        {
            "id": "25cad031-f20e-4ce2-8f75-578bf00034c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4c0145a-0ec2-4c1a-bc04-62062e6d60bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1016,
    "xorig": 508,
    "yorig": 38
}