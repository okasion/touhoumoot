{
    "id": "7705d1f3-7553-44fa-a38e-2d5396b328bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_orb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 1,
    "bbox_right": 28,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b600af7-bbb3-47a1-8b0b-35d48355ef31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7705d1f3-7553-44fa-a38e-2d5396b328bb",
            "compositeImage": {
                "id": "5d25128e-2744-4368-97f5-e16f0fcc9f5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b600af7-bbb3-47a1-8b0b-35d48355ef31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c03f7ba3-8824-43e4-b5d9-894eca48eeee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b600af7-bbb3-47a1-8b0b-35d48355ef31",
                    "LayerId": "ea6ea702-b3a7-4bef-a282-fb0c630fc5da"
                }
            ]
        },
        {
            "id": "cf256dcf-f653-4990-98ab-eb399fdadb30",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7705d1f3-7553-44fa-a38e-2d5396b328bb",
            "compositeImage": {
                "id": "1e23c36a-a120-4929-9f5b-c465a6c9d8b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cf256dcf-f653-4990-98ab-eb399fdadb30",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f83bacb-90ec-40e4-8093-ceb389d662fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cf256dcf-f653-4990-98ab-eb399fdadb30",
                    "LayerId": "ea6ea702-b3a7-4bef-a282-fb0c630fc5da"
                }
            ]
        },
        {
            "id": "617ddca1-8351-4209-bb7d-77221759e6b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7705d1f3-7553-44fa-a38e-2d5396b328bb",
            "compositeImage": {
                "id": "cf0628fe-a707-46f6-b004-90ac12f12512",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "617ddca1-8351-4209-bb7d-77221759e6b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1cff0d7-f41d-4070-b7b2-cf4165f2e354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "617ddca1-8351-4209-bb7d-77221759e6b4",
                    "LayerId": "ea6ea702-b3a7-4bef-a282-fb0c630fc5da"
                }
            ]
        },
        {
            "id": "865aec58-3adf-46ab-95e4-4ae8197cd4bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7705d1f3-7553-44fa-a38e-2d5396b328bb",
            "compositeImage": {
                "id": "39657bcd-79fd-477b-9392-5f7475b3c57c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "865aec58-3adf-46ab-95e4-4ae8197cd4bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71f33209-b05c-4fa6-9c1f-dc5e641e214e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "865aec58-3adf-46ab-95e4-4ae8197cd4bd",
                    "LayerId": "ea6ea702-b3a7-4bef-a282-fb0c630fc5da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "ea6ea702-b3a7-4bef-a282-fb0c630fc5da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7705d1f3-7553-44fa-a38e-2d5396b328bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 15
}