{
    "id": "79c344f4-af37-40eb-b4c0-879f037531a3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_playerleft",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 45,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f60e399b-18ae-4b90-ad84-9c4d65b075dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79c344f4-af37-40eb-b4c0-879f037531a3",
            "compositeImage": {
                "id": "a7c76593-af84-4585-8f9e-058fffbac557",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f60e399b-18ae-4b90-ad84-9c4d65b075dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c03fff4e-9e82-46ad-a8ca-bb051385e690",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f60e399b-18ae-4b90-ad84-9c4d65b075dc",
                    "LayerId": "4a466977-4cc9-4b76-8482-593a082951dd"
                }
            ]
        },
        {
            "id": "a3bc035b-17c3-499c-9820-d843ed9a0091",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79c344f4-af37-40eb-b4c0-879f037531a3",
            "compositeImage": {
                "id": "66312ad5-681f-4209-85c9-6f33a3d1b1a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3bc035b-17c3-499c-9820-d843ed9a0091",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cb7a83c-af62-4466-b545-7c059949a994",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3bc035b-17c3-499c-9820-d843ed9a0091",
                    "LayerId": "4a466977-4cc9-4b76-8482-593a082951dd"
                }
            ]
        },
        {
            "id": "1858840d-b59d-479e-95fb-3430e6e4ead5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79c344f4-af37-40eb-b4c0-879f037531a3",
            "compositeImage": {
                "id": "df6ff531-5537-4809-892f-e5a601c57929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1858840d-b59d-479e-95fb-3430e6e4ead5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "93dd06b9-7764-4978-b9a4-368fcd800a35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1858840d-b59d-479e-95fb-3430e6e4ead5",
                    "LayerId": "4a466977-4cc9-4b76-8482-593a082951dd"
                }
            ]
        },
        {
            "id": "6500d2ae-21b0-4e75-b76d-c5500b63287b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79c344f4-af37-40eb-b4c0-879f037531a3",
            "compositeImage": {
                "id": "44c2c31b-b592-4177-9693-954375f58106",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6500d2ae-21b0-4e75-b76d-c5500b63287b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aab35ca1-b684-48bf-aeb4-4df376d0285e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6500d2ae-21b0-4e75-b76d-c5500b63287b",
                    "LayerId": "4a466977-4cc9-4b76-8482-593a082951dd"
                }
            ]
        },
        {
            "id": "6bc026da-ee8b-4e35-8e83-07c87f5686a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79c344f4-af37-40eb-b4c0-879f037531a3",
            "compositeImage": {
                "id": "b10db597-f1be-4446-ac0a-ae8630bf89b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bc026da-ee8b-4e35-8e83-07c87f5686a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65e85964-db73-4910-8059-33b624154474",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bc026da-ee8b-4e35-8e83-07c87f5686a7",
                    "LayerId": "4a466977-4cc9-4b76-8482-593a082951dd"
                }
            ]
        },
        {
            "id": "29046b36-ff82-4027-a88d-73b95650cf04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79c344f4-af37-40eb-b4c0-879f037531a3",
            "compositeImage": {
                "id": "2044d9bf-be71-407c-b2a6-4db245b10b23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29046b36-ff82-4027-a88d-73b95650cf04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f794adf-72d4-46c8-aeee-4a7e4af2a1b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29046b36-ff82-4027-a88d-73b95650cf04",
                    "LayerId": "4a466977-4cc9-4b76-8482-593a082951dd"
                }
            ]
        },
        {
            "id": "ad79fd71-a211-43cc-8d76-497a431f5d25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79c344f4-af37-40eb-b4c0-879f037531a3",
            "compositeImage": {
                "id": "e498abc6-04eb-4f70-a573-65ed64c0915a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad79fd71-a211-43cc-8d76-497a431f5d25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d6cd384-03e2-493b-943e-8c08b867783e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad79fd71-a211-43cc-8d76-497a431f5d25",
                    "LayerId": "4a466977-4cc9-4b76-8482-593a082951dd"
                }
            ]
        },
        {
            "id": "ebb66cbf-3de5-4633-b8b4-944c9e93259c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79c344f4-af37-40eb-b4c0-879f037531a3",
            "compositeImage": {
                "id": "239f8c0a-7908-4b18-b5db-7667cc5355d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ebb66cbf-3de5-4633-b8b4-944c9e93259c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04abbad7-1d3b-45f6-ae12-196cf41416d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ebb66cbf-3de5-4633-b8b4-944c9e93259c",
                    "LayerId": "4a466977-4cc9-4b76-8482-593a082951dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4a466977-4cc9-4b76-8482-593a082951dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79c344f4-af37-40eb-b4c0-879f037531a3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 16
}