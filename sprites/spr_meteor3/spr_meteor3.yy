{
    "id": "3990e2ff-a64e-49f0-b124-5423a1ea559a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_meteor3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 42,
    "bbox_left": 2,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 3,
    "coltolerance": 96,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44862b07-3819-43f4-ba06-d6fcab4b5c97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3990e2ff-a64e-49f0-b124-5423a1ea559a",
            "compositeImage": {
                "id": "db3d73af-0cfd-4e05-b47a-609beca1a1d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44862b07-3819-43f4-ba06-d6fcab4b5c97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ee8bd6a-a9c9-4ede-8c20-ffd72074cbed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44862b07-3819-43f4-ba06-d6fcab4b5c97",
                    "LayerId": "6e7ab9bc-1734-411b-969e-b166544f1f37"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 43,
    "layers": [
        {
            "id": "6e7ab9bc-1734-411b-969e-b166544f1f37",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3990e2ff-a64e-49f0-b124-5423a1ea559a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 34,
    "xorig": 17,
    "yorig": 21
}