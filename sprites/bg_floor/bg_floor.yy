{
    "id": "f8bedae7-9aba-4e3e-9e7b-9d0984b8ef3e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_floor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1023,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "686ff839-8d7a-4a9f-ad5d-bfb2fedb95b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8bedae7-9aba-4e3e-9e7b-9d0984b8ef3e",
            "compositeImage": {
                "id": "b0f702db-bfbc-40bd-a835-95556b4e5cd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "686ff839-8d7a-4a9f-ad5d-bfb2fedb95b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff193d91-b0b5-49d0-ac39-7b49d25d7a96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "686ff839-8d7a-4a9f-ad5d-bfb2fedb95b0",
                    "LayerId": "af731605-537f-4487-ac25-0adad41a358f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1024,
    "layers": [
        {
            "id": "af731605-537f-4487-ac25-0adad41a358f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8bedae7-9aba-4e3e-9e7b-9d0984b8ef3e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 8,
    "yorig": 251
}