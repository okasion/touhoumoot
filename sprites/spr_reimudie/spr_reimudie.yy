{
    "id": "3a1d1f6f-b25e-4913-8792-7259d03bfa43",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_reimudie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a529895-491b-46e8-b494-482bd05497de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a1d1f6f-b25e-4913-8792-7259d03bfa43",
            "compositeImage": {
                "id": "38f74028-04ca-4310-900b-520595b31dfc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a529895-491b-46e8-b494-482bd05497de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb76642a-70e1-4bf4-9347-fae5b4f96849",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a529895-491b-46e8-b494-482bd05497de",
                    "LayerId": "8336a591-fc89-410b-87d4-1a6eeddebf9d"
                }
            ]
        },
        {
            "id": "3b69614d-d825-4c87-b869-6c04970061d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a1d1f6f-b25e-4913-8792-7259d03bfa43",
            "compositeImage": {
                "id": "68f42847-31bc-4182-a86e-eecdccc4c67a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b69614d-d825-4c87-b869-6c04970061d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "858ef486-7b5c-4d78-a54a-82cde11e90b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b69614d-d825-4c87-b869-6c04970061d8",
                    "LayerId": "8336a591-fc89-410b-87d4-1a6eeddebf9d"
                }
            ]
        },
        {
            "id": "a08a7402-03dd-4286-a347-82227798cb75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a1d1f6f-b25e-4913-8792-7259d03bfa43",
            "compositeImage": {
                "id": "db822936-afe6-43c4-9c7a-b01988a12324",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a08a7402-03dd-4286-a347-82227798cb75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e855db42-04f0-4cbc-8ba7-d1599b83859a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a08a7402-03dd-4286-a347-82227798cb75",
                    "LayerId": "8336a591-fc89-410b-87d4-1a6eeddebf9d"
                }
            ]
        },
        {
            "id": "a8f70163-4e47-4ba4-8c62-96721fdc3350",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a1d1f6f-b25e-4913-8792-7259d03bfa43",
            "compositeImage": {
                "id": "2cced218-b37a-4dc6-bf86-06e367e7f3b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8f70163-4e47-4ba4-8c62-96721fdc3350",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2e42e6f-97d4-419d-a381-0a869b468192",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8f70163-4e47-4ba4-8c62-96721fdc3350",
                    "LayerId": "8336a591-fc89-410b-87d4-1a6eeddebf9d"
                }
            ]
        },
        {
            "id": "30b96861-84c8-43f5-b6c3-ed14b195e593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a1d1f6f-b25e-4913-8792-7259d03bfa43",
            "compositeImage": {
                "id": "e9fce278-1bff-4c63-96f7-95878ff81958",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "30b96861-84c8-43f5-b6c3-ed14b195e593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35aff4af-dd08-4d22-9e8e-5f25f99883fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "30b96861-84c8-43f5-b6c3-ed14b195e593",
                    "LayerId": "8336a591-fc89-410b-87d4-1a6eeddebf9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8336a591-fc89-410b-87d4-1a6eeddebf9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a1d1f6f-b25e-4913-8792-7259d03bfa43",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 16
}