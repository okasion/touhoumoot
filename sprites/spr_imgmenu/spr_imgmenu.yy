{
    "id": "eebd9913-66c8-46d3-b60c-4ef575e4d41f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_imgmenu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": true,
    "frames": [
        {
            "id": "5895b8f4-8836-4f9f-a515-bd94b2493287",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eebd9913-66c8-46d3-b60c-4ef575e4d41f",
            "compositeImage": {
                "id": "f1047245-ba03-4977-a62c-d513b79d2a46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5895b8f4-8836-4f9f-a515-bd94b2493287",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d333077d-f405-481e-b241-b32c4d519399",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5895b8f4-8836-4f9f-a515-bd94b2493287",
                    "LayerId": "97d6ccb9-1bdf-42bd-98e9-ee044d8c2247"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "97d6ccb9-1bdf-42bd-98e9-ee044d8c2247",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eebd9913-66c8-46d3-b60c-4ef575e4d41f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 512,
    "yorig": 384
}