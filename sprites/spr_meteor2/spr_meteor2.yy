{
    "id": "33ed6b81-e220-43de-bd00-857593b47884",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_meteor2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 1,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 3,
    "coltolerance": 73,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "97b5c1a9-c53d-45ee-8b57-4028050465e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "33ed6b81-e220-43de-bd00-857593b47884",
            "compositeImage": {
                "id": "bc5f5d22-4bc9-499e-a07c-3af88eeba02b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97b5c1a9-c53d-45ee-8b57-4028050465e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d87acd4f-d726-4a15-8dce-faab6b667f4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97b5c1a9-c53d-45ee-8b57-4028050465e3",
                    "LayerId": "3eadb3e7-ff6f-4b3f-a355-2d3e69a4847a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 37,
    "layers": [
        {
            "id": "3eadb3e7-ff6f-4b3f-a355-2d3e69a4847a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "33ed6b81-e220-43de-bd00-857593b47884",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 18
}