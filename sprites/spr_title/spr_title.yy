{
    "id": "b2229be7-bad0-4925-bacb-a5a878dbdf77",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 0,
    "bbox_right": 226,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f26b5e30-9cf9-4a85-8afb-4273e913f4a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b2229be7-bad0-4925-bacb-a5a878dbdf77",
            "compositeImage": {
                "id": "dcf2d1f5-a7d8-4f5d-8a07-3d3b2915fc67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f26b5e30-9cf9-4a85-8afb-4273e913f4a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa23ad7f-bbc7-44d3-884c-1a261633a209",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f26b5e30-9cf9-4a85-8afb-4273e913f4a2",
                    "LayerId": "6335c495-30d0-40b5-9c7f-2118d9082803"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "6335c495-30d0-40b5-9c7f-2118d9082803",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b2229be7-bad0-4925-bacb-a5a878dbdf77",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 227,
    "xorig": 142,
    "yorig": 23
}