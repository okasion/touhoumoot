{
    "id": "7277da64-2fd5-4041-8458-eae431902df6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet_yukari_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3ac434ec-ce4d-42ea-8280-abe07ec3c7e4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7277da64-2fd5-4041-8458-eae431902df6",
            "compositeImage": {
                "id": "33c78826-51b4-42cf-9b4a-dbea9790df0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ac434ec-ce4d-42ea-8280-abe07ec3c7e4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70733f45-5452-4d02-bc93-f5679f466ce7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ac434ec-ce4d-42ea-8280-abe07ec3c7e4",
                    "LayerId": "e952e73f-36ec-413e-9557-4b69314cce34"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "e952e73f-36ec-413e-9557-4b69314cce34",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7277da64-2fd5-4041-8458-eae431902df6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 15
}