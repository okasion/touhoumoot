{
    "id": "bceb7dcf-a199-4d67-a45d-6c00e2032109",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet_yukari_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 3,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d84e3701-f14e-4fcf-9d84-5fe8f0334869",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bceb7dcf-a199-4d67-a45d-6c00e2032109",
            "compositeImage": {
                "id": "d336547c-8d28-4933-a6f8-6bde237f563b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d84e3701-f14e-4fcf-9d84-5fe8f0334869",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4230fa1-426a-410b-a751-413b6f59a3d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d84e3701-f14e-4fcf-9d84-5fe8f0334869",
                    "LayerId": "8d43f786-d657-4123-8abc-3ff764f4d9d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 33,
    "layers": [
        {
            "id": "8d43f786-d657-4123-8abc-3ff764f4d9d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bceb7dcf-a199-4d67-a45d-6c00e2032109",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 16
}