{
    "id": "787c00f9-360e-47d8-bbc7-d7fe0acd09dc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_meteor1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 3,
    "coltolerance": 101,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "471e4349-af9f-438c-b5b0-0dea0f6ab6ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "787c00f9-360e-47d8-bbc7-d7fe0acd09dc",
            "compositeImage": {
                "id": "67e6ec1e-3680-4fce-b76d-992e3b572885",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "471e4349-af9f-438c-b5b0-0dea0f6ab6ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "975c1763-7026-4e80-b6fa-5f1fc0a1ddb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "471e4349-af9f-438c-b5b0-0dea0f6ab6ac",
                    "LayerId": "199b5a63-1e85-4ede-bc66-b071d543819e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "199b5a63-1e85-4ede-bc66-b071d543819e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "787c00f9-360e-47d8-bbc7-d7fe0acd09dc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}