{
    "id": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_heat_bar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 0,
    "bbox_right": 9,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23bd685e-8659-4967-91f4-3095f2beb340",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "10136bff-9779-481a-8767-33857fa556d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23bd685e-8659-4967-91f4-3095f2beb340",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e52c3a37-ce40-4fb0-8a9e-021e42be2ce6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23bd685e-8659-4967-91f4-3095f2beb340",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "ba0ea557-d58e-43cb-832b-61aacb835bed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "18168215-0b8d-452e-ad30-04751ce19df7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba0ea557-d58e-43cb-832b-61aacb835bed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3aec714-5021-452c-b193-1f19bb27b58a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba0ea557-d58e-43cb-832b-61aacb835bed",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "08c2da74-9dfc-47c3-92ac-383f6a933245",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "86127305-c957-44af-9f25-a166909c362c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08c2da74-9dfc-47c3-92ac-383f6a933245",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7564f15e-3efc-4768-9757-123f964d74b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08c2da74-9dfc-47c3-92ac-383f6a933245",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "c342a35f-3fdf-4ea1-a759-cdd92de0509c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "749d8ace-c5d2-4474-92c0-5a2cf4234e03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c342a35f-3fdf-4ea1-a759-cdd92de0509c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e176f014-4012-4254-a610-823e1b0ab59b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c342a35f-3fdf-4ea1-a759-cdd92de0509c",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "818ae587-ed9d-4879-8f53-d2253c00b942",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "491fe922-b6f0-4fcc-a3e8-5751da134f3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "818ae587-ed9d-4879-8f53-d2253c00b942",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44942f4a-c55a-42b9-9441-a00fac8ed79c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "818ae587-ed9d-4879-8f53-d2253c00b942",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "aaef957e-0ef7-4f42-af4e-9287b1819995",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "f2b91d9a-73be-4d7b-a791-ec24adf3edb3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaef957e-0ef7-4f42-af4e-9287b1819995",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6f514f6-29a1-4736-a5cc-1350708b5074",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaef957e-0ef7-4f42-af4e-9287b1819995",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "09cf8882-5bde-486f-8732-b3b08b08a639",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "cf7a5a6c-0d80-47c1-98ab-2101e4c2220c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09cf8882-5bde-486f-8732-b3b08b08a639",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc031dee-e9bd-4385-8813-e631ca60b43f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09cf8882-5bde-486f-8732-b3b08b08a639",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "1a3dfc3e-fd52-4757-b096-63359e187e91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "cacd4757-44ae-4487-907a-58f1c4eb63a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a3dfc3e-fd52-4757-b096-63359e187e91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecab2600-f6c3-4f8d-8960-d9e4ce8768b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a3dfc3e-fd52-4757-b096-63359e187e91",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "dc951ccb-9ef2-4e79-88b6-bb13824252e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "e3ea0282-6f09-4b3d-ae16-5855d56b3a6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dc951ccb-9ef2-4e79-88b6-bb13824252e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf6815d6-d2f8-4202-9d2d-da83a699cf64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dc951ccb-9ef2-4e79-88b6-bb13824252e7",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "02f3d1c7-b4e2-4447-8867-a7fb55c6770f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "38982d20-6563-44ca-bc8c-21378eb6f3c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02f3d1c7-b4e2-4447-8867-a7fb55c6770f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c82d73d-43e0-4428-96e0-67da24a0823d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02f3d1c7-b4e2-4447-8867-a7fb55c6770f",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "88cad639-cf24-4997-8c4b-4691735e48f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "ad2d82a0-7eca-4d2f-9706-41df40a99943",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88cad639-cf24-4997-8c4b-4691735e48f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e911240-bc72-497e-82d9-526488b8ac20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88cad639-cf24-4997-8c4b-4691735e48f9",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "cd2f9645-bbb5-4f7c-8528-546eef514906",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "99b10543-8383-4a40-b3bb-93fe27b542a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd2f9645-bbb5-4f7c-8528-546eef514906",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85166132-4130-4676-b78e-3c9879c0a0ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd2f9645-bbb5-4f7c-8528-546eef514906",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "81f5808e-e2c1-4234-994f-dea9d4873519",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "01787678-94a8-4815-a5a0-b02bbab369cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81f5808e-e2c1-4234-994f-dea9d4873519",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "183ac9c3-1fdf-48f5-8795-c52d6fecf042",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81f5808e-e2c1-4234-994f-dea9d4873519",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "3abe266f-8d2b-4484-983b-16619ba0c5ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "e681eea2-51aa-4842-870d-584a1b60b3ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3abe266f-8d2b-4484-983b-16619ba0c5ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4aa88b50-204d-4206-a231-25256ead4237",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3abe266f-8d2b-4484-983b-16619ba0c5ae",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "a7a8c879-404e-4c48-a2f5-103aaae17411",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "cf3166b1-469a-49fc-9f9e-9f6c732b1685",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7a8c879-404e-4c48-a2f5-103aaae17411",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26cbe0c1-27d4-418f-b412-8227c1c6d1f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7a8c879-404e-4c48-a2f5-103aaae17411",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "82a02849-410d-4a67-ac90-b1b0b33a082b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "0dc2025c-b864-4bd8-96aa-366907d32643",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "82a02849-410d-4a67-ac90-b1b0b33a082b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9b090ff-75ba-4436-a526-f1f249b65bc6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "82a02849-410d-4a67-ac90-b1b0b33a082b",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "9f4f3389-bdec-4ba0-808e-175d7abd8cae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "a4fc7ad3-760c-463e-bee8-8e18002a9394",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9f4f3389-bdec-4ba0-808e-175d7abd8cae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69b9e19f-7fb1-41da-b2c1-941381c16c73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9f4f3389-bdec-4ba0-808e-175d7abd8cae",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "66782209-20f4-4d2d-bdb5-2d11f855437a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "fce8bf37-39e8-43e4-85f3-b74ff7cba68e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66782209-20f4-4d2d-bdb5-2d11f855437a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a781d04f-e306-44f3-8832-f4d9e79b8fa1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66782209-20f4-4d2d-bdb5-2d11f855437a",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "d03a8137-49ab-4729-9b3f-b0895e1e20b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "bf84029a-facf-4d5c-8622-23e89a57e08f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d03a8137-49ab-4729-9b3f-b0895e1e20b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca160ae1-0240-4793-9eb6-8c8b2482c309",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d03a8137-49ab-4729-9b3f-b0895e1e20b1",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "f8d75eb3-0e76-4402-ad54-73c8ed656465",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "f7597245-4550-419d-a90a-a24e3a14c097",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8d75eb3-0e76-4402-ad54-73c8ed656465",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce591606-556c-4cfa-8908-023a9b88365f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8d75eb3-0e76-4402-ad54-73c8ed656465",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        },
        {
            "id": "a65804c7-1e0e-43cb-9a06-cd44ad030fe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "compositeImage": {
                "id": "f74c969b-b3cd-4fcd-9049-f26e4a15af0c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a65804c7-1e0e-43cb-9a06-cd44ad030fe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad493adb-4737-45a6-9fde-be5aaeee93f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a65804c7-1e0e-43cb-9a06-cd44ad030fe5",
                    "LayerId": "272ec472-cf23-45c2-94b6-7f477ee651d8"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 82,
    "layers": [
        {
            "id": "272ec472-cf23-45c2-94b6-7f477ee651d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f4ee7738-1b77-4340-9116-9966b13bc3b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 5,
    "yorig": 41
}