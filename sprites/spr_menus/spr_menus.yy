{
    "id": "2d276aed-e480-424a-aa86-9a67c480d7f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_menus",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 0,
    "bbox_right": 269,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "128e9a00-66ec-4667-89bf-a61cbca3fa26",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d276aed-e480-424a-aa86-9a67c480d7f9",
            "compositeImage": {
                "id": "6da439cc-3977-46c9-b12e-19ad456cae11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "128e9a00-66ec-4667-89bf-a61cbca3fa26",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17f8c93f-b133-45bc-8fc8-5ad39cd16b45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "128e9a00-66ec-4667-89bf-a61cbca3fa26",
                    "LayerId": "6c102afa-389f-4e4d-af7a-99ad754142e0"
                }
            ]
        },
        {
            "id": "665b4ab1-5d0d-4cdf-b9c7-44614c093813",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d276aed-e480-424a-aa86-9a67c480d7f9",
            "compositeImage": {
                "id": "011af42e-b2e8-4735-833c-b52691b0c7c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "665b4ab1-5d0d-4cdf-b9c7-44614c093813",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f03d9ddf-c0c5-459f-aaba-02b5d3fde55b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "665b4ab1-5d0d-4cdf-b9c7-44614c093813",
                    "LayerId": "6c102afa-389f-4e4d-af7a-99ad754142e0"
                }
            ]
        },
        {
            "id": "08eb765a-c942-406c-a66e-9cc107ed91a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d276aed-e480-424a-aa86-9a67c480d7f9",
            "compositeImage": {
                "id": "c1b6f32f-27ba-4d85-80f0-2aec4a42a826",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08eb765a-c942-406c-a66e-9cc107ed91a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fe55da5-758b-4431-ab0c-28e773d08ca7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08eb765a-c942-406c-a66e-9cc107ed91a2",
                    "LayerId": "6c102afa-389f-4e4d-af7a-99ad754142e0"
                }
            ]
        },
        {
            "id": "493ab97e-b649-4419-bc58-733b4441bb5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d276aed-e480-424a-aa86-9a67c480d7f9",
            "compositeImage": {
                "id": "04881113-efd1-443a-a239-6315b60e0430",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "493ab97e-b649-4419-bc58-733b4441bb5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0da03bdd-05e3-4f26-bc0f-5af9c10c84fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "493ab97e-b649-4419-bc58-733b4441bb5d",
                    "LayerId": "6c102afa-389f-4e4d-af7a-99ad754142e0"
                }
            ]
        },
        {
            "id": "5bd19d29-07ff-4fbc-afbf-a75e0e0777da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d276aed-e480-424a-aa86-9a67c480d7f9",
            "compositeImage": {
                "id": "46ed29c5-f7be-4afd-9321-174f6d135ea5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5bd19d29-07ff-4fbc-afbf-a75e0e0777da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea14eb0c-b632-41b0-bfc4-74647dc2784f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5bd19d29-07ff-4fbc-afbf-a75e0e0777da",
                    "LayerId": "6c102afa-389f-4e4d-af7a-99ad754142e0"
                }
            ]
        },
        {
            "id": "73347934-8a30-42a1-a4d5-6b936746dc5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d276aed-e480-424a-aa86-9a67c480d7f9",
            "compositeImage": {
                "id": "e92d946b-b154-42dd-a29d-e9ae13a3dc4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73347934-8a30-42a1-a4d5-6b936746dc5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b2eaf71-9678-48ca-ae9f-b2070cc19473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73347934-8a30-42a1-a4d5-6b936746dc5e",
                    "LayerId": "6c102afa-389f-4e4d-af7a-99ad754142e0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 52,
    "layers": [
        {
            "id": "6c102afa-389f-4e4d-af7a-99ad754142e0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d276aed-e480-424a-aa86-9a67c480d7f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 270,
    "xorig": 135,
    "yorig": 26
}