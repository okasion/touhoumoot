{
    "id": "863df86d-4e53-40b8-883d-da6b967ac994",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_barrier",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 719,
    "bbox_left": 0,
    "bbox_right": 1279,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c33b882a-7021-409a-81e5-8a5ccc758e5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "863df86d-4e53-40b8-883d-da6b967ac994",
            "compositeImage": {
                "id": "0662dee2-f6ca-4e15-9014-f3f31d333085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c33b882a-7021-409a-81e5-8a5ccc758e5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eabc6bae-7a01-4606-8884-cf76df5ecfc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c33b882a-7021-409a-81e5-8a5ccc758e5a",
                    "LayerId": "eb741680-369b-46ef-bfda-66f1969760f6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 720,
    "layers": [
        {
            "id": "eb741680-369b-46ef-bfda-66f1969760f6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "863df86d-4e53-40b8-883d-da6b967ac994",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1280,
    "xorig": 0,
    "yorig": 0
}