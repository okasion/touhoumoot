{
    "id": "965d23cb-2fb8-469e-8d0c-551398077d8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 14,
    "bbox_right": 49,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "64c9d084-c6d2-4fe6-a202-3d3a195f1e61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "965d23cb-2fb8-469e-8d0c-551398077d8a",
            "compositeImage": {
                "id": "b2eb08f1-61c8-4b85-9a3b-84fb55a5f1b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "64c9d084-c6d2-4fe6-a202-3d3a195f1e61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb58a4ca-98e7-4f3d-b1b7-744313e81e79",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "64c9d084-c6d2-4fe6-a202-3d3a195f1e61",
                    "LayerId": "2e747469-655e-49d4-9632-3ee6f233a5dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2e747469-655e-49d4-9632-3ee6f233a5dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "965d23cb-2fb8-469e-8d0c-551398077d8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 32
}